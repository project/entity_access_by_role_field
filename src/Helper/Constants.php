<?php

namespace Drupal\entity_access_by_role_field\Helper;

/**
 * Common used constants for the 'entity_access_by_role_field' module.
 */
class Constants {

  public const FIELD_TYPE = 'entity_access_by_role_field';

  public const ACCESS_ALLOWED = 'allowed';
  public const ACCESS_FORBIDDEN = 'forbidden';

  public const OPERATION_VIEW = 'view';
  public const OPERATION_VIEW_UNPUBLISHED = 'view_unpublished';
  public const OPERATION_EDIT = 'update';
  public const OPERATION_DELETE = 'delete';

  public const FALLBACK_NEUTRAL = 'neutral';
  public const FALLBACK_ALLOWED = 'allowed';
  public const FALLBACK_FORBIDDEN = 'forbidden';

  public const GLOBAL_PERMISSION = 'bypass entity_access_by_role_field permissions';

}
