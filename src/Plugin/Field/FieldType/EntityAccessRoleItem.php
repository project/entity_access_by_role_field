<?php

namespace Drupal\entity_access_by_role_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\OptionsProviderInterface;
use Drupal\entity_access_by_role_field\Helper\Constants;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;

/**
 * Plugin implementation of the 'entity_access_by_role_field' field type.
 *
 * @FieldType(
 *   id = "entity_access_by_role_field",
 *   label = @Translation("Entity Access by Role"),
 *   module = "entity_access_by_role_field",
 *   description = @Translation("A field to specify access behavior for its entity."),
 *   category = "access",
 *   default_widget = "default_entity_access_by_role_field_widget",
 *   default_formatter = "default_entity_access_by_role_field_formatter",
 * )
 */
class EntityAccessRoleItem extends FieldItemBase implements OptionsProviderInterface {

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return 'role_id';
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'role_id' => [
          'type' => 'varchar',
          'length' => 255,
          // Role selection is optional, falling back to NEUTRAL access, if no
          // role is selected:
          'not null' => FALSE,
        ],
        'access' => [
          'type' => 'varchar',
          'length' => 10,
          // Access selection (allow / deny) is always required:
          'not null' => TRUE,
        ],
      ],
      'indexes' => [
        'role_id' => ['role_id'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $role = $this->get('role_id')->getValue();
    $access = $this->get('access')->getValue();
    return empty($access) && (empty($role) || $role === '_none');
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // Important: The order is relevant here! role_id is our main property
    // used by the widget!
    $properties['role_id'] = DataDefinition::create('string')
      ->setLabel(t('Role'))
      // Role selection is optional, falling back to NEUTRAL access, if no role
      // is selected:
      ->setRequired(FALSE);

    $properties['access'] = DataDefinition::create('string')
      ->setLabel(t('Access'))
      // Access selection (allow / deny) is always required:
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function getPossibleValues(AccountInterface $account = NULL) {
    // Return all available role ids array ['administrator']:
    return array_keys(array_map(function (RoleInterface $role) {
      return $role->label();
    }, Role::loadMultiple()));
  }

  /**
   * {@inheritdoc}
   */
  public function getPossibleOptions(AccountInterface $account = NULL) {
    // Return associative array of role ids and names:
    $rolesToLoad = $account ? $account->getRoles() : NULL;
    return array_map(function (RoleInterface $role) {
      return $role->label();
    }, Role::loadMultiple($rolesToLoad));
  }

  /**
   * {@inheritdoc}
   */
  public function getSettableValues(AccountInterface $account = NULL) {
    // Same result as self::getPossibleValues().
    return $this->getPossibleValues($account);
  }

  /**
   * {@inheritdoc}
   */
  public function getSettableOptions(AccountInterface $account = NULL) {
    // Same result as self::getPossibleOptions().
    return $this->getPossibleOptions($account);
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $roles = array_map(function (RoleInterface $role) {
      return $role->label();
    }, Role::loadMultiple());
    $roleCount = count($roles);
    $values['role_id'] = array_rand(array_values($roles), mt_rand(0, $roleCount));
    $values['access'] = array_rand([
      Constants::ACCESS_FORBIDDEN,
      Constants::ACCESS_ALLOWED,
    ]);
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];
    // The settings for which operations the role is authenticated to use on
    // this entity:
    $elements['operations'] = [
      "#type" => "checkboxes",
      "#title" => $this->t("The allowed operations to access this entity"),
      '#options' => [
        Constants::OPERATION_VIEW => $this->t('View Published'),
        Constants::OPERATION_VIEW_UNPUBLISHED => $this->t('View Unpublished'),
        Constants::OPERATION_EDIT => $this->t('Edit'),
        Constants::OPERATION_DELETE => $this->t('Delete'),
      ],
      // '#disabled' => TRUE,
      '#required' => TRUE,
      '#description' => $this->t('Select the allowed operations to access this entity'),
      '#default_value' => $this->getSetting('operations'),
    ];

    // The settings for what should happen if no role is selected,
    // when creating an entity:
    $elements['empty_roles_access_fallback'] = [
      "#type" => "radios",
      "#title" => $this->t("Access for this entity, if no role is selected"),
      '#options' => [
        Constants::FALLBACK_NEUTRAL => $this->t('Neutral Access - Let other modules set the access'),
        Constants::FALLBACK_ALLOWED => $this->t('Access Allowed - All roles have access'),
        Constants::FALLBACK_FORBIDDEN => $this->t('Access Forbidden - No roles have access'),
      ],
      "#description" => $this->t("Select the access permissions, if no roles are selected at all, BE CAREFUL SWITCHING FROM NEUTRAL ACCESS!"),
      '#default_value' => $this->getSetting('empty_roles_access_fallback'),
    ];

    return $elements + parent::fieldSettingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'operations' => [Constants::OPERATION_VIEW],
      'empty_roles_access_fallback' => Constants::FALLBACK_NEUTRAL,
    ] + parent::defaultFieldSettings();
  }

}
