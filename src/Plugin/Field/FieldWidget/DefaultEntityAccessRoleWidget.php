<?php

namespace Drupal\entity_access_by_role_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsButtonsWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entity_access_by_role_field\Helper\Constants;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;

/**
 * Plugin implementation of the 'entity_access_by_role_field' widget.
 *
 * @FieldWidget(
 *   id = "default_entity_access_by_role_field_widget",
 *   module = "entity_access_by_role_field",
 *   label = @Translation("Allow / Deny Roles Widget"),
 *   field_types = {
 *     "entity_access_by_role_field"
 *   },
 *   multiple_values = TRUE
 * )
 */
class DefaultEntityAccessRoleWidget extends OptionsButtonsWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $fieldCardinality = $items->getFieldDefinition()->getFieldStorageDefinition()->getCardinality();

    $newElement = [];
    $newElement['details'] = [
      '#type' => 'details',
      '#title' => $element['#title'],
      '#open' => TRUE,
      '#tree' => TRUE,
      '#description' => $element['#description'],
    ];

    $operations = _entity_access_by_role_field_filter_operations($this->fieldDefinition->getSetting('operations'));
    $operationsString = implode(' ' . $this->t('or') . ' ', $operations);
    $newElement['details']['access'] = [
      '#title' => $this->t('Allow / restrict role access to %operations this entity:', ['%operations' => $operationsString]),
      '#type' => 'radios',
      '#options' => [
        Constants::ACCESS_ALLOWED => $this->t('Allow the selected below'),
        Constants::ACCESS_FORBIDDEN => $this->t('Restrict the selected below'),
      ],
      // Set access allowed as fallback default:
      '#default_value' => $items->getValue()[$delta]['access'] ?? Constants::ACCESS_ALLOWED,
      '#required' => TRUE,
      '#description' => $this->t('Allow or restrict access by role for the selected operations: <strong>@operations</strong>', ['@operations' => $operationsString]),
    ];

    // We set a different title for the field and use the field name for
    // the details wrapper:
    $maxRoles = $fieldCardinality === FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED ? $this->t('unlimited') : $fieldCardinality;
    $element['#title'] = $this->t('When the user has the following roles (max. %cardinality):', ['%cardinality' => $maxRoles]);
    $element['#description'] = $this->t("Select the roles that should be allowed / denied access. They are combined by OR.<br /><strong>If no roles are selected, the access will fallback to '@fallbackValue'.</strong>", ['@fallbackValue' => $this->fieldDefinition->getSetting('empty_roles_access_fallback')]);
    // Overwrite the parent element options, as not all roles are being used:
    $element['#options'] = $this->getRoleOptions();
    // Re-add the original element below the details wrapper:
    $newElement['details']['role_id'] = $element;
    // Set the default value based on the field cardinality
    // (radios / checkboxes):
    $roleIdDefaultValue = NULL;

    if ($fieldCardinality === 1) {
      // Add '_none' as an additional option for radios:
      $newElement['details']['role_id']['#options'] += ['_none' => $this->t('None')];
      // As we use radios, the fallback value is '_none' instead of NULL:
      $roleIdDefaultValue = $items->getValue()[$delta]['role_id'] ?? '_none';
    }
    else {
      $defaultValues = [];
      foreach ($items->getValue() as $widgetSettings) {
        if (isset($widgetSettings['role_id'])) {
          $defaultValues[] = $widgetSettings['role_id'];
        }
      }
      $roleIdDefaultValue = $defaultValues;
    }
    $newElement['details']['role_id']['#default_value'] = $roleIdDefaultValue;
    return $newElement;
  }

  /**
   * Retrieves the role options for the entity access role widget.
   *
   * @return array
   *   An array of role options.
   */
  protected function getRoleOptions(): array {
    return array_map(function (RoleInterface $role) {
      return $role->label();
    }, Role::loadMultiple());
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    /*
     * The submit handler expects a structure like in database:
     * [
     *    0 => [
     *      'role_id' => 'authenticated',
     *      'access' => 'allowed',
     *    ],
     *    1 => [
     *      'role_id' => 'administrator',
     *      'access' => 'allowed',
     *    ],
     *    ...
     * ]
     */
    $massagedValues = [];
    if (!empty($values)) {
      // Set roles by delta:
      $massagedValues = $values['details']['role_id'];

      // Use array_filter to deep check if the array is empty:
      if (empty(array_filter($massagedValues))) {
        $massagedValues[0]['access'] = $values['details']['access'] ?? Constants::ACCESS_ALLOWED;
        $massagedValues[0]['role_id'] = NULL;
      }
      else {
        foreach ($massagedValues as $key => $value) {
          $massagedValues[$key]['access'] = $values['details']['access'];
        }
      }
      // Append access value into each role array:
    }
    return $massagedValues;
  }

}
