<?php

namespace Drupal\entity_access_by_role_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the default 'entity_access_by_role_field' formatter.
 *
 * @FieldFormatter(
 *   id = "default_entity_access_by_role_field_formatter",
 *   module = "entity_access_by_role_field",
 *   label = @Translation("No output formatter"),
 *   field_types = {
 *     "entity_access_by_role_field"
 *   }
 * )
 */
class DefaultEntityAccessRoleFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    return $elements;
  }

}
