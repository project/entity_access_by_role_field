<?php

namespace Drupal\entity_access_by_role_field\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the default 'entity_access_by_role_field' formatter.
 *
 * @FieldFormatter(
 *   id = "debug_entity_access_by_role_field_formatter",
 *   module = "entity_access_by_role_field",
 *   label = @Translation("Debug output formatter (Show Role: Access)"),
 *   field_types = {
 *     "entity_access_by_role_field"
 *   }
 * )
 */
class DebugEntityAccessRoleFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    foreach ($items as $delta => $item) {
      // Render each element as markup.
      $element[$delta] = ['#markup' => Html::escape($item->role_id) . ': ' . Html::escape($item->access)];
    }
    return $element;
  }

}
