<?php

namespace Drupal\entity_access_by_role_field\Exceptions;

/**
 * Class defining a custom "RoleOptionNotFoundException".
 */
class RoleOptionNotFoundException extends \Exception {
}
