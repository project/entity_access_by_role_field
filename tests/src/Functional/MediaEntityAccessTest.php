<?php

namespace Drupal\Tests\entity_access_by_role_field\Functional;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\entity_access_by_role_field\Helper\Constants;
use Drupal\media\Entity\Media;
use Drupal\Tests\media\Traits\MediaTypeCreationTrait;

/**
 * This class provides access tests for entity_access_by_role_field media.
 *
 * @group entity_access_by_role_field
 */
class MediaEntityAccessTest extends EntityAccessTestBase {
  use MediaTypeCreationTrait;

  /**
   * {@inheritDoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Enable URLs for media:
    \Drupal::configFactory()
      ->getEditable('media.settings')
      ->set('standalone_url', TRUE)
      ->save(TRUE);

    $this->container->get('router.builder')->rebuild();

    // Create Media Type:
    $this->createMediaType('test', [
      'id' => 'test',
      'label' => 'Test Media Type',
    ]);

    // Create the adminUser:
    $this->adminUser = $this->createUser([]);
    $this->adminUser->addRole($this->createAdminRole('admin', 'admin'));
    $this->adminUser->save();

    // Create the global permissionUser:
    $this->globalPermissionUser = $this->createUser([
      'bypass entity_access_by_role_field permissions',
    ]);
    // Create the authenticatedUser:
    $this->authenticatedUser = $this->createUser([]);

  }

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'entity_access_by_role_field',
    'field',
    'media',
    // Needed for 'test' media types:
    'media_test_source',
  ];

  /**
   * Tests access on a allowed foreign media entity by an registered user.
   */
  public function testAccessOnAllowedForeignMedia() {
    $this->drupalLogin($this->authenticatedUser);
    $this->createAccessFieldTypeOnEntityBundle('media', 'test', $this->fieldName, [Constants::OPERATION_VIEW => Constants::OPERATION_VIEW], Constants::FALLBACK_NEUTRAL, FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);
    // Create media.
    $media = Media::create([
      'bundle' => 'test',
      'name' => 'Unnamed',
    ]);
    $media->save();
    // Add our field:
    $this->setEntityAccessByRoleFieldOnEntity($media, $this->fieldName, ['authenticated'], Constants::ACCESS_ALLOWED);
    // Check expected access with our field:
    $this->drupalGet('/media/' . $media->id());
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('/media/' . $media->id() . '/edit');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet('/media/' . $media->id() . '/delete');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Tests access on a forbidden foreign media entity by an registered user.
   */
  public function testAccessOnForbiddenForeignMedia() {
    $this->drupalLogin($this->authenticatedUser);
    $this->createAccessFieldTypeOnEntityBundle('media', 'test', $this->fieldName, [Constants::OPERATION_VIEW => Constants::OPERATION_VIEW], Constants::FALLBACK_NEUTRAL, FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);
    // Create media.
    $media = Media::create([
      'bundle' => 'test',
      'name' => 'Unnamed',
    ]);
    $media->save();
    // Add our field:
    $this->setEntityAccessByRoleFieldOnEntity($media, $this->fieldName, ['authenticated'], Constants::ACCESS_FORBIDDEN);
    // Check expected access with our field:
    $this->drupalGet('/media/' . $media->id());
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet('/media/' . $media->id() . '/edit');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet('/media/' . $media->id() . '/delete');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Tests access on an allowed media entity owned by the user.
   */
  public function testAccessOnAllowedOwnMedia() {
    $this->drupalLogin($this->authenticatedUser);
    $this->createAccessFieldTypeOnEntityBundle('media', 'test', $this->fieldName, [Constants::OPERATION_VIEW => Constants::OPERATION_VIEW], Constants::FALLBACK_NEUTRAL, FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);
    // Create media.
    $media = Media::create([
      'bundle' => 'test',
      'name' => 'Test media created by the authenticated user',
      'uid' => $this->authenticatedUser->id(),
    ]);
    $media->save();
    // Add our field:
    $this->setEntityAccessByRoleFieldOnEntity($media, $this->fieldName, ['authenticated'], Constants::ACCESS_ALLOWED);
    // Check expected access with our field:
    $this->drupalGet('/media/' . $media->id());
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('/media/' . $media->id() . '/edit');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet('/media/' . $media->id() . '/delete');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Tests access on a forbidden media entity owned by the user.
   */
  public function testAccessOnForbiddenOwnMedia() {
    $this->drupalLogin($this->authenticatedUser);
    $this->createAccessFieldTypeOnEntityBundle('media', 'test', $this->fieldName, [Constants::OPERATION_VIEW => Constants::OPERATION_VIEW], Constants::FALLBACK_NEUTRAL, FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);
    // Create media.
    $media = Media::create([
      'bundle' => 'test',
      'name' => 'Test media created by the authenticated user',
      'uid' => $this->authenticatedUser->id(),
    ]);
    $media->save();
    // Add our field:
    $this->setEntityAccessByRoleFieldOnEntity($media, $this->fieldName, ['authenticated'], Constants::ACCESS_FORBIDDEN);
    // Check expected access with our field:
    $this->drupalGet('/media/' . $media->id());
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet('/media/' . $media->id() . '/edit');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet('/media/' . $media->id() . '/delete');
    $this->assertSession()->statusCodeEquals(403);
  }

}
