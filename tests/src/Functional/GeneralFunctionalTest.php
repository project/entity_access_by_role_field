<?php

namespace Drupal\Tests\entity_access_by_role_field\Functional;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\entity_access_by_role_field\Helper\Constants;

/**
 * This class provides general tests for the entity_access_by_role_field module.
 *
 * @group entity_access_by_role_field
 */
class GeneralFunctionalTest extends EntityAccessTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'entity_access_by_role_field',
    'node',
    'field',
    'field_ui',
  ];

  /**
   * {@inheritDoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Create the adminUser:
    $this->adminUser = $this->createUser([]);
    $this->adminUser->addRole($this->createAdminRole('admin', 'admin'));
    $this->adminUser->save();
  }

  /**
   * Setup node permissions and entities for node specific tests.
   */
  protected function setupNodeDependencies() {
    // NOTE, that all users need to have the 'access content' permission,
    // otherwise, they are not able to access a node in any way!
    // Create the global permissionUser:
    $this->globalPermissionUser = $this->createUser([
      'access content',
      'bypass entity_access_by_role_field permissions',
    ]);

    // Create the authenticatedUser:
    $this->authenticatedUser = $this->createUser(['access content']);

    // Create the testRoleUser:
    $this->testRoleUser = $this->createUser([
      'access content',
    ]);
    $testRoleRid = $this->createRole(['access content'], 'test_role', 'test_role');
    $this->testRoleUser->addRole($testRoleRid);
    $this->testRoleUser->save();

    $this->drupalCreateContentType(['type' => 'article']);
  }

  /**
   * Retrieves a list of all role ids.
   */
  protected function getUserRoleIds() {
    $roleIds = [];
    $user_roles = \Drupal::entityTypeManager()->getStorage('user_role')->loadMultiple();
    foreach ($user_roles as $user_role) {
      $roleIds[] = str_replace('_', '-', $user_role->id());
    }
    return $roleIds;
  }

  /**
   * Tests the default value of the field with a cardinality of 1.
   */
  public function testDefaultValueRoleIdCardinalityOne() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    $this->setupNodeDependencies();
    $this->drupalLogin($this->adminUser);

    // Test the default value with a cardinality of 1:
    $this->createAccessFieldTypeOnEntityBundle('node', 'article', $this->fieldName, [Constants::OPERATION_EDIT => Constants::OPERATION_EDIT], Constants::FALLBACK_NEUTRAL, 1);

    // Simply create a new article without adjusting the field settings, and
    // check if "_none" is the default value:
    $this->drupalGet('/node/add/article');
    $session->statusCodeEquals(200);
    $session->elementAttributeContains('css', '#edit-field-access-details-role-id-none', 'checked', 'checked');

    // Set some default values and check if they are set correctly:
    $this->drupalGet('/admin/structure/types/manage/article/fields/node.article.field_access');
    $session->statusCodeEquals(200);
    $page->fillField('edit-default-value-input-field-access-details-access-allowed', 'allowed');
    $page->fillField('edit-default-value-input-field-access-details-role-id-authenticated', 'authenticated');
    $page->pressButton('Save settings');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Saved field_access configuration.');

    // Reload the page and check if the default settings are present on the
    // field settings page:
    $this->drupalGet('/admin/structure/types/manage/article/fields/node.article.field_access');
    $session->statusCodeEquals(200);
    $session->elementAttributeContains('css', '#edit-default-value-input-field-access-details-access-allowed', 'checked', 'checked');
    $session->elementAttributeContains('css', '#edit-default-value-input-field-access-details-role-id-authenticated', 'checked', 'checked');

    // Create a new article, the default values should be set accordingly:
    $this->drupalGet('/node/add/article');
    $session->elementAttributeContains('css', '#edit-field-access-details-access-allowed', 'checked', 'checked');
    $session->elementAttributeContains('css', '#edit-field-access-details-role-id-authenticated', 'checked', 'checked');
    $page->fillField('edit-title-0-value', 'Test article');
    $page->pressButton('Save');

    // Revisit the article, and see if the default values are still set:
    $this->drupalGet('/node/1/edit');
    $session->elementAttributeContains('css', '#edit-field-access-details-access-allowed', 'checked', 'checked');
    $session->elementAttributeContains('css', '#edit-field-access-details-role-id-authenticated', 'checked', 'checked');
    // Overwrite the field default values, with entity specific values:
    $page->fillField('edit-field-access-details-role-id-admin', 'admin');
    $page->pressButton('Save');
    $session->statusCodeEquals(200);
    $session->pageTextContains('article Test article has been updated.');

    // Revisit the article, and see if the field settings default value does
    // not overwrite the entity's default value:
    $this->drupalGet('/node/1/edit');
    $session->elementAttributeContains('css', '#edit-field-access-details-role-id-admin', 'checked', 'checked');

    // Check if the field settings are not overwritten by the entity's default:
    $this->drupalGet('/admin/structure/types/manage/article/fields/node.article.field_access');
    $session->statusCodeEquals(200);
    $session->elementAttributeContains('css', '#edit-default-value-input-field-access-details-role-id-authenticated', 'checked', 'checked');

    // Revisit the field settings and set a different default value:
    $this->drupalGet('/admin/structure/types/manage/article/fields/node.article.field_access');
    $session->statusCodeEquals(200);
    $page->fillField('edit-default-value-input-field-access-details-role-id-anonymous', 'anonymous');
    $page->pressButton('Save settings');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Saved field_access configuration.');

    // Revisit the article, and see if the field settings default value does
    // not overwrite the entity's default value:
    $this->drupalGet('/node/1/edit');
    $session->elementAttributeContains('css', '#edit-field-access-details-role-id-admin', 'checked', 'checked');

    // Check if the field settings are still set to anonymous:
    $this->drupalGet('/admin/structure/types/manage/article/fields/node.article.field_access');
    $session->statusCodeEquals(200);
    $session->elementAttributeContains('css', '#edit-default-value-input-field-access-details-role-id-anonymous', 'checked', 'checked');

    // Uncheck the default value:
    $this->drupalGet('/admin/structure/types/manage/article/fields/node.article.field_access');
    $session->statusCodeEquals(200);
    $page->fillField('edit-default-value-input-field-access-details-role-id-none', '_none');
    $page->pressButton('Save settings');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Saved field_access configuration.');

    // Create a new article, the default value should be set to none now:
    $this->drupalGet('/node/add/article');
    $session->statusCodeEquals(200);
    $session->elementAttributeContains('css', '#edit-field-access-details-role-id-none', 'checked', 'checked');
  }

  /**
   * Tests a single default value of the field with a cardinality of 2.
   */
  public function testSingleDefaultValueRoleIdCardinalityMultiple() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    $this->setupNodeDependencies();
    $this->drupalLogin($this->adminUser);
    $roleIds = $this->getUserRoleIds();

    // Test the default value with a cardinality of 1:
    $this->createAccessFieldTypeOnEntityBundle('node', 'article', $this->fieldName, [Constants::OPERATION_EDIT => Constants::OPERATION_EDIT], Constants::FALLBACK_NEUTRAL, 2);

    // Simply create a new article without adjusting the field settings, and
    // check that no default value is set and no error appears:
    $this->drupalGet('/node/add/article');
    $session->statusCodeEquals(200);
    foreach ($roleIds as $roleId) {
      $session->elementAttributeNotExists('css', '#edit-field-access-details-role-id-' . $roleId, 'checked');
    }

    // Set a single default values and check if they are set correctly:
    $this->drupalGet('/admin/structure/types/manage/article/fields/node.article.field_access');
    $session->statusCodeEquals(200);
    $page->fillField('edit-default-value-input-field-access-details-access-allowed', 'allowed');
    $page->checkField('edit-default-value-input-field-access-details-role-id-authenticated');
    $page->pressButton('Save settings');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Saved field_access configuration.');

    // Reload the page and check if the single default setting is present on
    // the field settings page:
    $this->drupalGet('/admin/structure/types/manage/article/fields/node.article.field_access');
    $session->statusCodeEquals(200);
    $session->elementAttributeContains('css', '#edit-default-value-input-field-access-details-access-allowed', 'checked', 'checked');
    $session->elementAttributeContains('css', '#edit-default-value-input-field-access-details-role-id-authenticated', 'checked', 'checked');

    // Create a new article, the single default value should be set accordingly:
    $this->drupalGet('/node/add/article');
    $session->elementAttributeContains('css', '#edit-field-access-details-access-allowed', 'checked', 'checked');
    $session->elementAttributeContains('css', '#edit-field-access-details-role-id-authenticated', 'checked', 'checked');
    $page->fillField('edit-title-0-value', 'Test article');
    $page->pressButton('Save');

    // Revisit the article, and see if the default values are still set:
    $this->drupalGet('/node/1/edit');
    $session->elementAttributeContains('css', '#edit-field-access-details-access-allowed', 'checked', 'checked');
    $session->elementAttributeContains('css', '#edit-field-access-details-role-id-authenticated', 'checked', 'checked');
    // Overwrite the field default values, with entity specific values:
    $page->checkField('edit-field-access-details-role-id-admin');
    $page->uncheckField('edit-field-access-details-role-id-authenticated');
    $page->pressButton('Save');
    $session->statusCodeEquals(200);
    $session->pageTextContains('article Test article has been updated.');

    // Revisit the article, and see if the field settings default value does
    // not overwrite the entity's default value:
    $this->drupalGet('/node/1/edit');
    $session->elementAttributeContains('css', '#edit-field-access-details-role-id-admin', 'checked', 'checked');
    $session->elementAttributeNotExists('css', '#edit-field-access-details-role-id-authenticated', 'checked');

    // Check if the field settings are not overwritten by the entity's default:
    $this->drupalGet('/admin/structure/types/manage/article/fields/node.article.field_access');
    $session->statusCodeEquals(200);
    $session->elementAttributeContains('css', '#edit-default-value-input-field-access-details-role-id-authenticated', 'checked', 'checked');
    $session->elementAttributeNotExists('css', '#edit-default-value-input-field-access-details-role-id-admin', 'checked');

    // Revisit the field settings and set a different default value:
    $this->drupalGet('/admin/structure/types/manage/article/fields/node.article.field_access');
    $session->statusCodeEquals(200);
    $page->checkField('edit-default-value-input-field-access-details-role-id-anonymous');
    $page->uncheckField('edit-default-value-input-field-access-details-role-id-authenticated');
    $page->pressButton('Save settings');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Saved field_access configuration.');

    // Revisit the article, and see if the field settings default value does
    // not overwrite the entity's default value:
    $this->drupalGet('/node/1/edit');
    $session->elementAttributeContains('css', '#edit-field-access-details-role-id-admin', 'checked', 'checked');
    $session->elementAttributeNotExists('css', '#edit-field-access-details-role-id-authenticated', 'checked');
    $session->elementAttributeNotExists('css', '#edit-field-access-details-role-id-anonymous', 'checked');

    // Check if the field settings are still set to anonymous:
    $this->drupalGet('/admin/structure/types/manage/article/fields/node.article.field_access');
    $session->statusCodeEquals(200);
    $session->elementAttributeContains('css', '#edit-default-value-input-field-access-details-role-id-anonymous', 'checked', 'checked');
    $session->elementAttributeNotExists('css', '#edit-default-value-input-field-access-details-role-id-authenticated', 'checked');
    $session->elementAttributeNotExists('css', '#edit-default-value-input-field-access-details-role-id-admin', 'checked');

    // Uncheck all the default values on the field settings page:
    $this->drupalGet('/admin/structure/types/manage/article/fields/node.article.field_access');
    $session->statusCodeEquals(200);
    $page->uncheckField('edit-default-value-input-field-access-details-role-id-anonymous');
    $page->pressButton('Save settings');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Saved field_access configuration.');

    // Revisit the field settings page and confirm all checkboxes are unchecked:
    $this->drupalGet('/admin/structure/types/manage/article/fields/node.article.field_access');
    $session->statusCodeEquals(200);
    foreach ($roleIds as $roleId) {
      $session->elementAttributeNotExists('css', '#edit-default-value-input-field-access-details-role-id-' . $roleId, 'checked');
    }

    // Create a new article, no checkbox should be checked:
    $this->drupalGet('/node/add/article');
    $session->statusCodeEquals(200);
    foreach ($roleIds as $roleId) {
      $session->elementAttributeNotExists('css', '#edit-field-access-details-role-id-' . $roleId, 'checked');
    }

    // Go to an existing article, the default values shouldn't have changed
    // here:
    $this->drupalGet('/node/1/edit');
    $session->elementAttributeContains('css', '#edit-field-access-details-role-id-admin', 'checked', 'checked');
    $session->elementAttributeNotExists('css', '#edit-field-access-details-role-id-authenticated', 'checked');
    $session->elementAttributeNotExists('css', '#edit-field-access-details-role-id-anonymous', 'checked');

    // Revisit the field settings page and check a couple of checkboxes:
    $this->drupalGet('/admin/structure/types/manage/article/fields/node.article.field_access');
    $page->fillField('edit-default-value-input-field-access-details-access-allowed', 'allowed');
    $page->checkField('edit-default-value-input-field-access-details-role-id-anonymous');
    $page->checkField('edit-default-value-input-field-access-details-role-id-authenticated');
    $page->pressButton('Save settings');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Saved field_access configuration.');
    // Uncheck all fields, save and reload the page
    // to see, if no attribute is checked:
    $this->drupalGet('/node/1/edit');
    $session->elementAttributeContains('css', '#edit-field-access-details-role-id-admin', 'checked', 'checked');
    $session->elementAttributeNotExists('css', '#edit-field-access-details-role-id-authenticated', 'checked');
    $session->elementAttributeNotExists('css', '#edit-field-access-details-role-id-anonymous', 'checked');
    $page->uncheckField('edit-field-access-details-role-id-admin');
    $page->pressButton('Save');
    $session->statusCodeEquals(200);
    $session->pageTextContains('article Test article has been updated.');
    $this->drupalGet('/node/1/edit');
    $session->elementAttributeNotExists('css', '#edit-field-access-details-role-id-admin', 'checked');
    $session->elementAttributeNotExists('css', '#edit-field-access-details-role-id-authenticated', 'checked');
    $session->elementAttributeNotExists('css', '#edit-field-access-details-role-id-anonymous', 'checked');
  }

  /**
   * Tests multiple default value of the field with a cardinality of 2.
   */
  public function testMultipleDefaultValuesRoleIdCardinalityMultiple() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    $this->setupNodeDependencies();
    $this->drupalLogin($this->adminUser);

    // Test the default value with a cardinality of 1:
    $this->createAccessFieldTypeOnEntityBundle('node', 'article', $this->fieldName, [Constants::OPERATION_EDIT => Constants::OPERATION_EDIT], Constants::FALLBACK_NEUTRAL, 2);

    // Set multiple default values and check if they are set correctly:
    $this->drupalGet('/admin/structure/types/manage/article/fields/node.article.field_access');
    $session->statusCodeEquals(200);
    $page->fillField('edit-default-value-input-field-access-details-access-allowed', 'allowed');

    $page->checkField('edit-default-value-input-field-access-details-role-id-authenticated');
    $page->checkField('edit-default-value-input-field-access-details-role-id-anonymous');
    $page->pressButton('Save settings');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Saved field_access configuration.');

    // Reload the page and check if the multiple default setting are present on
    // the field settings page:
    $this->drupalGet('/admin/structure/types/manage/article/fields/node.article.field_access');
    $session->statusCodeEquals(200);
    $session->elementAttributeContains('css', '#edit-default-value-input-field-access-details-access-allowed', 'checked', 'checked');
    $session->elementAttributeContains('css', '#edit-default-value-input-field-access-details-role-id-authenticated', 'checked', 'checked');
    $session->elementAttributeContains('css', '#edit-default-value-input-field-access-details-role-id-anonymous', 'checked', 'checked');

    // Create a new article, the single default value should be set accordingly:
    $this->drupalGet('/node/add/article');
    $session->elementAttributeContains('css', '#edit-field-access-details-access-allowed', 'checked', 'checked');
    $session->elementAttributeContains('css', '#edit-field-access-details-role-id-authenticated', 'checked', 'checked');
    $session->elementAttributeContains('css', '#edit-field-access-details-role-id-anonymous', 'checked', 'checked');
    $page->fillField('edit-title-0-value', 'Test article');
    $page->pressButton('Save');

    // Revisit the article, and see if the default values are still set:
    $this->drupalGet('/node/1/edit');
    $session->elementAttributeContains('css', '#edit-field-access-details-access-allowed', 'checked', 'checked');
    $session->elementAttributeContains('css', '#edit-field-access-details-role-id-authenticated', 'checked', 'checked');
    $session->elementAttributeContains('css', '#edit-field-access-details-role-id-anonymous', 'checked', 'checked');
    // Overwrite the field default values, with entity specific values:
    $page->checkField('edit-field-access-details-role-id-admin');
    $page->checkField('edit-field-access-details-role-id-anonymous');
    $page->uncheckField('edit-field-access-details-role-id-authenticated');
    $page->pressButton('Save');
    $session->statusCodeEquals(200);
    $session->pageTextContains('article Test article has been updated.');

    // Revisit the article, and see if the field settings default values does
    // not overwrite the entity's default values:
    $this->drupalGet('/node/1/edit');
    $session->elementAttributeContains('css', '#edit-field-access-details-role-id-admin', 'checked', 'checked');
    $session->elementAttributeContains('css', '#edit-field-access-details-role-id-anonymous', 'checked', 'checked');
    $session->elementAttributeNotExists('css', '#edit-field-access-details-role-id-authenticated', 'checked');

    // Check if the field settings are not overwritten by the entity's default:
    $this->drupalGet('/admin/structure/types/manage/article/fields/node.article.field_access');
    $session->statusCodeEquals(200);
    $session->elementAttributeContains('css', '#edit-default-value-input-field-access-details-role-id-authenticated', 'checked', 'checked');
    $session->elementAttributeContains('css', '#edit-default-value-input-field-access-details-role-id-anonymous', 'checked', 'checked');
    $session->elementAttributeNotExists('css', '#edit-default-value-input-field-access-details-role-id-admin', 'checked');

    // Revisit the field settings and set a different default value:
    $this->drupalGet('/admin/structure/types/manage/article/fields/node.article.field_access');
    $session->statusCodeEquals(200);
    $page->checkField('edit-default-value-input-field-access-details-role-id-admin');
    $page->uncheckField('edit-default-value-input-field-access-details-role-id-anonymous');
    $page->uncheckField('edit-default-value-input-field-access-details-role-id-authenticated');
    $page->pressButton('Save settings');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Saved field_access configuration.');

    // Revisit the article, and see if the field settings default value does
    // not overwrite the entity's default value:
    $this->drupalGet('/node/1/edit');
    $session->elementAttributeContains('css', '#edit-field-access-details-role-id-admin', 'checked', 'checked');
    $session->elementAttributeContains('css', '#edit-field-access-details-role-id-anonymous', 'checked', 'checked');
    $session->elementAttributeNotExists('css', '#edit-field-access-details-role-id-authenticated', 'checked');

    // Check if the field settings are still set accordingly:
    $this->drupalGet('/admin/structure/types/manage/article/fields/node.article.field_access');
    $session->statusCodeEquals(200);
    $session->elementAttributeContains('css', '#edit-default-value-input-field-access-details-role-id-admin', 'checked', 'checked');
    $session->elementAttributeNotExists('css', '#edit-default-value-input-field-access-details-role-id-authenticated', 'checked');
    $session->elementAttributeNotExists('css', '#edit-default-value-input-field-access-details-role-id-anonymous', 'checked');

    // Revisit the field settings page and check a couple of checkboxes:
    $page->fillField('edit-default-value-input-field-access-details-access-allowed', 'allowed');
    $page->checkField('edit-default-value-input-field-access-details-role-id-authenticated');
    $page->pressButton('Save settings');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Saved field_access configuration.');
    // Uncheck all fields, save and reload the page
    // to see, if no attribute is checked:
    $this->drupalGet('/node/1/edit');
    $session->elementAttributeContains('css', '#edit-field-access-details-role-id-admin', 'checked', 'checked');
    $session->elementAttributeContains('css', '#edit-field-access-details-role-id-anonymous', 'checked', 'checked');
    $session->elementAttributeNotExists('css', '#edit-field-access-details-role-id-authenticated', 'checked');
    $page->uncheckField('edit-field-access-details-role-id-admin');
    $page->uncheckField('edit-field-access-details-role-id-anonymous');
    $page->pressButton('Save');
    $session->statusCodeEquals(200);
    $session->pageTextContains('article Test article has been updated.');
    $this->drupalGet('/node/1/edit');
    $session->elementAttributeNotExists('css', '#edit-field-access-details-role-id-admin', 'checked');
    $session->elementAttributeNotExists('css', '#edit-field-access-details-role-id-authenticated', 'checked');
    $session->elementAttributeNotExists('css', '#edit-field-access-details-role-id-anonymous', 'checked');
  }

  /**
   * Tests a single default value of the field with an unlimited cardinality .
   */
  public function testSingleDefaultValueRoleIdCardinalityUnlimited() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    $this->setupNodeDependencies();
    $this->drupalLogin($this->adminUser);
    $roleIds = $this->getUserRoleIds();

    // Test the default value with a cardinality of 1:
    $this->createAccessFieldTypeOnEntityBundle('node', 'article', $this->fieldName, [Constants::OPERATION_EDIT => Constants::OPERATION_EDIT], Constants::FALLBACK_NEUTRAL, FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);

    // Simply create a new article without adjusting the field settings, and
    // check that no default value is set and no error appears:
    $this->drupalGet('/node/add/article');
    $session->statusCodeEquals(200);
    foreach ($roleIds as $roleId) {
      $session->elementAttributeNotExists('css', '#edit-field-access-details-role-id-' . $roleId, 'checked');
    }

    // Set a single default values and check if they are set correctly:
    $this->drupalGet('/admin/structure/types/manage/article/fields/node.article.field_access');
    $session->statusCodeEquals(200);
    $page->fillField('edit-default-value-input-field-access-details-access-allowed', 'allowed');
    $page->checkField('edit-default-value-input-field-access-details-role-id-authenticated');
    $page->pressButton('Save settings');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Saved field_access configuration.');

    // Reload the page and check if the single default setting is present on
    // the field settings page:
    $this->drupalGet('/admin/structure/types/manage/article/fields/node.article.field_access');
    $session->statusCodeEquals(200);
    $session->elementAttributeContains('css', '#edit-default-value-input-field-access-details-access-allowed', 'checked', 'checked');
    $session->elementAttributeContains('css', '#edit-default-value-input-field-access-details-role-id-authenticated', 'checked', 'checked');

    // Create a new article, the single default value should be set accordingly:
    $this->drupalGet('/node/add/article');
    $session->elementAttributeContains('css', '#edit-field-access-details-access-allowed', 'checked', 'checked');
    $session->elementAttributeContains('css', '#edit-field-access-details-role-id-authenticated', 'checked', 'checked');
    $page->fillField('edit-title-0-value', 'Test article');
    $page->pressButton('Save');

    // Revisit the article, and see if the default values are still set:
    $this->drupalGet('/node/1/edit');
    $session->elementAttributeContains('css', '#edit-field-access-details-access-allowed', 'checked', 'checked');
    $session->elementAttributeContains('css', '#edit-field-access-details-role-id-authenticated', 'checked', 'checked');
    // Overwrite the field default values, with entity specific values:
    $page->checkField('edit-field-access-details-role-id-admin');
    $page->uncheckField('edit-field-access-details-role-id-authenticated');
    $page->pressButton('Save');
    $session->statusCodeEquals(200);
    $session->pageTextContains('article Test article has been updated.');

    // Revisit the article, and see if the field settings default value does
    // not overwrite the entity's default value:
    $this->drupalGet('/node/1/edit');
    $session->elementAttributeContains('css', '#edit-field-access-details-role-id-admin', 'checked', 'checked');
    $session->elementAttributeNotExists('css', '#edit-field-access-details-role-id-authenticated', 'checked');

    // Check if the field settings are not overwritten by the entity's default:
    $this->drupalGet('/admin/structure/types/manage/article/fields/node.article.field_access');
    $session->statusCodeEquals(200);
    $session->elementAttributeContains('css', '#edit-default-value-input-field-access-details-role-id-authenticated', 'checked', 'checked');
    $session->elementAttributeNotExists('css', '#edit-default-value-input-field-access-details-role-id-admin', 'checked');

    // Revisit the field settings and set a different default value:
    $this->drupalGet('/admin/structure/types/manage/article/fields/node.article.field_access');
    $session->statusCodeEquals(200);
    $page->checkField('edit-default-value-input-field-access-details-role-id-anonymous');
    $page->uncheckField('edit-default-value-input-field-access-details-role-id-authenticated');
    $page->pressButton('Save settings');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Saved field_access configuration.');

    // Revisit the article, and see if the field settings default value does
    // not overwrite the entity's default value:
    $this->drupalGet('/node/1/edit');
    $session->elementAttributeContains('css', '#edit-field-access-details-role-id-admin', 'checked', 'checked');
    $session->elementAttributeNotExists('css', '#edit-field-access-details-role-id-authenticated', 'checked');
    $session->elementAttributeNotExists('css', '#edit-field-access-details-role-id-anonymous', 'checked');

    // Check if the field settings are still set to anonymous:
    $this->drupalGet('/admin/structure/types/manage/article/fields/node.article.field_access');
    $session->statusCodeEquals(200);
    $session->elementAttributeContains('css', '#edit-default-value-input-field-access-details-role-id-anonymous', 'checked', 'checked');
    $session->elementAttributeNotExists('css', '#edit-default-value-input-field-access-details-role-id-authenticated', 'checked');
    $session->elementAttributeNotExists('css', '#edit-default-value-input-field-access-details-role-id-admin', 'checked');

    // Uncheck all the default values on the field settings page:
    $this->drupalGet('/admin/structure/types/manage/article/fields/node.article.field_access');
    $session->statusCodeEquals(200);
    $page->uncheckField('edit-default-value-input-field-access-details-role-id-anonymous');
    $page->pressButton('Save settings');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Saved field_access configuration.');

    // Revisit the field settings page and confirm all checkboxes are unchecked:
    $this->drupalGet('/admin/structure/types/manage/article/fields/node.article.field_access');
    $session->statusCodeEquals(200);
    foreach ($roleIds as $roleId) {
      $session->elementAttributeNotExists('css', '#edit-default-value-input-field-access-details-role-id-' . $roleId, 'checked');
    }

    // Create a new article, no checkbox should be checked:
    $this->drupalGet('/node/add/article');
    $session->statusCodeEquals(200);
    foreach ($roleIds as $roleId) {
      $session->elementAttributeNotExists('css', '#edit-field-access-details-role-id-' . $roleId, 'checked');
    }

    // Go to an existing article, the default values shouldn't have changed
    // here:
    $this->drupalGet('/node/1/edit');
    $session->elementAttributeContains('css', '#edit-field-access-details-role-id-admin', 'checked', 'checked');
    $session->elementAttributeNotExists('css', '#edit-field-access-details-role-id-authenticated', 'checked');
    $session->elementAttributeNotExists('css', '#edit-field-access-details-role-id-anonymous', 'checked');

    // Revisit the field settings page and check a couple of checkboxes:
    $this->drupalGet('/admin/structure/types/manage/article/fields/node.article.field_access');
    $page->fillField('edit-default-value-input-field-access-details-access-allowed', 'allowed');
    $page->checkField('edit-default-value-input-field-access-details-role-id-anonymous');
    $page->checkField('edit-default-value-input-field-access-details-role-id-authenticated');
    $page->pressButton('Save settings');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Saved field_access configuration.');
    // Uncheck all fields, save and reload the page
    // to see, if no attribute is checked:
    $this->drupalGet('/node/1/edit');
    $session->elementAttributeContains('css', '#edit-field-access-details-role-id-admin', 'checked', 'checked');
    $session->elementAttributeNotExists('css', '#edit-field-access-details-role-id-authenticated', 'checked');
    $session->elementAttributeNotExists('css', '#edit-field-access-details-role-id-anonymous', 'checked');
    $page->uncheckField('edit-field-access-details-role-id-admin');
    $page->pressButton('Save');
    $session->statusCodeEquals(200);
    $session->pageTextContains('article Test article has been updated.');
    $this->drupalGet('/node/1/edit');
    $session->elementAttributeNotExists('css', '#edit-field-access-details-role-id-admin', 'checked');
    $session->elementAttributeNotExists('css', '#edit-field-access-details-role-id-authenticated', 'checked');
    $session->elementAttributeNotExists('css', '#edit-field-access-details-role-id-anonymous', 'checked');
  }

  /**
   * Tests multiple default value of the field with a cardinality.
   */
  public function testMultipleDefaultValuesRoleIdCardinalityUnlimited() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    $this->setupNodeDependencies();
    $this->drupalLogin($this->adminUser);

    // Test the default value with a cardinality of 1:
    $this->createAccessFieldTypeOnEntityBundle('node', 'article', $this->fieldName, [Constants::OPERATION_EDIT => Constants::OPERATION_EDIT], Constants::FALLBACK_NEUTRAL, FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);

    // Set multiple default values and check if they are set correctly:
    $this->drupalGet('/admin/structure/types/manage/article/fields/node.article.field_access');
    $session->statusCodeEquals(200);
    $page->fillField('edit-default-value-input-field-access-details-access-allowed', 'allowed');

    $page->checkField('edit-default-value-input-field-access-details-role-id-authenticated');
    $page->checkField('edit-default-value-input-field-access-details-role-id-anonymous');
    $page->pressButton('Save settings');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Saved field_access configuration.');

    // Reload the page and check if the multiple default setting are present on
    // the field settings page:
    $this->drupalGet('/admin/structure/types/manage/article/fields/node.article.field_access');
    $session->statusCodeEquals(200);
    $session->elementAttributeContains('css', '#edit-default-value-input-field-access-details-access-allowed', 'checked', 'checked');
    $session->elementAttributeContains('css', '#edit-default-value-input-field-access-details-role-id-authenticated', 'checked', 'checked');
    $session->elementAttributeContains('css', '#edit-default-value-input-field-access-details-role-id-anonymous', 'checked', 'checked');

    // Create a new article, the default values should be set accordingly:
    $this->drupalGet('/node/add/article');
    $session->elementAttributeContains('css', '#edit-field-access-details-access-allowed', 'checked', 'checked');
    $session->elementAttributeContains('css', '#edit-field-access-details-role-id-authenticated', 'checked', 'checked');
    $session->elementAttributeContains('css', '#edit-field-access-details-role-id-anonymous', 'checked', 'checked');
    $page->fillField('edit-title-0-value', 'Test article');
    $page->pressButton('Save');

    // Revisit the article, and see if the default values are still set:
    $this->drupalGet('/node/1/edit');
    $session->elementAttributeContains('css', '#edit-field-access-details-access-allowed', 'checked', 'checked');
    $session->elementAttributeContains('css', '#edit-field-access-details-role-id-authenticated', 'checked', 'checked');
    $session->elementAttributeContains('css', '#edit-field-access-details-role-id-anonymous', 'checked', 'checked');
    // Overwrite the field default values, with entity specific values:
    $page->checkField('edit-field-access-details-role-id-admin');
    $page->checkField('edit-field-access-details-role-id-anonymous');
    $page->uncheckField('edit-field-access-details-role-id-authenticated');
    $page->pressButton('Save');
    $session->statusCodeEquals(200);
    $session->pageTextContains('article Test article has been updated.');

    // Revisit the article, and see if the field settings default values does
    // not overwrite the entity's default values:
    $this->drupalGet('/node/1/edit');
    $session->elementAttributeContains('css', '#edit-field-access-details-role-id-admin', 'checked', 'checked');
    $session->elementAttributeContains('css', '#edit-field-access-details-role-id-anonymous', 'checked', 'checked');
    $session->elementAttributeNotExists('css', '#edit-field-access-details-role-id-authenticated', 'checked');

    // Check if the field settings are not overwritten by the entity's default:
    $this->drupalGet('/admin/structure/types/manage/article/fields/node.article.field_access');
    $session->statusCodeEquals(200);
    $session->elementAttributeContains('css', '#edit-default-value-input-field-access-details-role-id-authenticated', 'checked', 'checked');
    $session->elementAttributeContains('css', '#edit-default-value-input-field-access-details-role-id-anonymous', 'checked', 'checked');
    $session->elementAttributeNotExists('css', '#edit-default-value-input-field-access-details-role-id-admin', 'checked');

    // Revisit the field settings and set a different default value:
    $this->drupalGet('/admin/structure/types/manage/article/fields/node.article.field_access');
    $session->statusCodeEquals(200);
    $page->checkField('edit-default-value-input-field-access-details-role-id-admin');
    $page->uncheckField('edit-default-value-input-field-access-details-role-id-anonymous');
    $page->uncheckField('edit-default-value-input-field-access-details-role-id-authenticated');
    $page->pressButton('Save settings');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Saved field_access configuration.');

    // Revisit the article, and see if the field settings default value does
    // not overwrite the entity's default value:
    $this->drupalGet('/node/1/edit');
    $session->elementAttributeContains('css', '#edit-field-access-details-role-id-admin', 'checked', 'checked');
    $session->elementAttributeContains('css', '#edit-field-access-details-role-id-anonymous', 'checked', 'checked');
    $session->elementAttributeNotExists('css', '#edit-field-access-details-role-id-authenticated', 'checked');

    // Check if the field settings are still set accordingly:
    $this->drupalGet('/admin/structure/types/manage/article/fields/node.article.field_access');
    $session->statusCodeEquals(200);
    $session->elementAttributeContains('css', '#edit-default-value-input-field-access-details-role-id-admin', 'checked', 'checked');
    $session->elementAttributeNotExists('css', '#edit-default-value-input-field-access-details-role-id-authenticated', 'checked');
    $session->elementAttributeNotExists('css', '#edit-default-value-input-field-access-details-role-id-anonymous', 'checked');

    // Revisit the field settings page and check a couple of checkboxes:
    $page->fillField('edit-default-value-input-field-access-details-access-allowed', 'allowed');
    $page->checkField('edit-default-value-input-field-access-details-role-id-authenticated');
    $page->pressButton('Save settings');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Saved field_access configuration.');
    // Uncheck all fields, save and reload the page
    // to see, if no attribute is checked:
    $this->drupalGet('/node/1/edit');
    $session->elementAttributeContains('css', '#edit-field-access-details-role-id-admin', 'checked', 'checked');
    $session->elementAttributeContains('css', '#edit-field-access-details-role-id-anonymous', 'checked', 'checked');
    $session->elementAttributeNotExists('css', '#edit-field-access-details-role-id-authenticated', 'checked');
    $page->uncheckField('edit-field-access-details-role-id-admin');
    $page->uncheckField('edit-field-access-details-role-id-anonymous');
    $page->pressButton('Save');
    $session->statusCodeEquals(200);
    $session->pageTextContains('article Test article has been updated.');
    $this->drupalGet('/node/1/edit');
    $session->elementAttributeNotExists('css', '#edit-field-access-details-role-id-admin', 'checked');
    $session->elementAttributeNotExists('css', '#edit-field-access-details-role-id-authenticated', 'checked');
    $session->elementAttributeNotExists('css', '#edit-field-access-details-role-id-anonymous', 'checked');
  }

  /**
   * Tests the default value of the "access" field.
   */
  public function testDefaultValueAccess() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    $this->setupNodeDependencies();
    $this->drupalLogin($this->adminUser);
    $roleIds = $this->getUserRoleIds();

    $this->createAccessFieldTypeOnEntityBundle('node', 'article', $this->fieldName, [Constants::OPERATION_EDIT => Constants::OPERATION_EDIT], Constants::FALLBACK_NEUTRAL, FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);

    $this->drupalGet('/admin/structure/types/manage/article/fields/node.article.field_access');
    $session->statusCodeEquals(200);
    // On initialization, "Allow the selected below" should be used as fallback:
    $session->elementAttributeContains('css', '#edit-default-value-input-field-access-details-access-allowed', 'checked', 'checked');

    // Make sure no roles are selected:
    foreach ($roleIds as $roleId) {
      $session->elementAttributeNotExists('css', '#edit-default-value-input-field-access-details-role-id-' . $roleId, 'checked');
    }

    // Just save, and see if the default value is still set:
    $page->pressButton('Save settings');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Saved field_access configuration.');

    // Revisit the settings page and check if the value is set:
    $this->drupalGet('/admin/structure/types/manage/article/fields/node.article.field_access');
    $session->elementAttributeContains('css', '#edit-default-value-input-field-access-details-access-allowed', 'checked', 'checked');

    // Make sure, that still no roles are selected:
    foreach ($roleIds as $roleId) {
      $session->elementAttributeNotExists('css', '#edit-default-value-input-field-access-details-role-id-' . $roleId, 'checked');
    }

    // Create a new article, the default value should be the fallback:
    $this->drupalGet('/node/add/article');
    $session->elementAttributeContains('css', '#edit-field-access-details-access-allowed', 'checked', 'checked');

    // Make, that also here no roles are selected:
    foreach ($roleIds as $roleId) {
      $session->elementAttributeNotExists('css', '#edit-field-access-details-role-id-' . $roleId, 'checked');
    }

    // Visit the field settings page and change the access behaviour:
    $this->drupalGet('/admin/structure/types/manage/article/fields/node.article.field_access');
    $page->fillField('edit-default-value-input-field-access-details-access-forbidden', 'forbidden');
    $page->pressButton('Save settings');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Saved field_access configuration.');

    // Revisit the settings page and check if the value is set:
    $this->drupalGet('/admin/structure/types/manage/article/fields/node.article.field_access');
    $session->elementAttributeContains('css', '#edit-default-value-input-field-access-details-access-forbidden', 'checked', 'checked');

    // Create a new article, the default value should be the fallback:
    $this->drupalGet('/node/add/article');
    $session->elementAttributeContains('css', '#edit-field-access-details-access-forbidden', 'checked', 'checked');
  }

}
