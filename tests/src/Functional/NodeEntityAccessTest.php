<?php

namespace Drupal\Tests\entity_access_by_role_field\Functional;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\entity_access_by_role_field\Helper\Constants;

/**
 * This class provides access tests for entity_access_by_role_field node.
 *
 * @group entity_access_by_role_field
 */
class NodeEntityAccessTest extends EntityAccessTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'entity_access_by_role_field',
    'node',
    'field',
    'field_ui',
  ];

  /**
   * {@inheritDoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Create the adminUser:
    $this->adminUser = $this->createUser([]);
    $this->adminUser->addRole($this->createAdminRole('admin', 'admin'));
    $this->adminUser->save();

    // NOTE, that all users need to have the 'access content' permission,
    // otherwise, they are not able to access a node in any way!
    // Create the global permissionUser:
    $this->globalPermissionUser = $this->createUser([
      'access content',
      'bypass entity_access_by_role_field permissions',
    ]);

    // Create the authenticatedUser:
    $this->authenticatedUser = $this->createUser(['access content']);

    // Create the testRoleUser:
    $this->testRoleUser = $this->createUser([
      'access content',
    ]);
    $testRoleRid = $this->createRole(['access content'], 'test_role', 'test_role');
    $this->testRoleUser->addRole($testRoleRid);
    $this->testRoleUser->save();

    $this->drupalCreateContentType(['type' => 'article']);
  }

  /**
   * Test if global ignore_permission ignores field settings.
   */
  public function testGlobalPermissionOnForbiddenNode() {
    $this->drupalLogin($this->globalPermissionUser);
    $this->createAccessFieldTypeOnEntityBundle('node', 'article', $this->fieldName, [Constants::OPERATION_EDIT => Constants::OPERATION_EDIT], Constants::FALLBACK_NEUTRAL, FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);
    $node = $this->createNode(['title' => 'Test Article', 'type' => 'article']);
    $this->setEntityAccessByRoleFieldOnEntity($node, $this->fieldName, ['authenticated'], Constants::ACCESS_FORBIDDEN);
    // Check expected access with our field:
    $this->drupalGet('/node/' . $node->id());
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('/node/' . $node->id() . '/edit');
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('/node/' . $node->id() . '/delete');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Test if accessing node as admin, ignores field settings.
   */
  public function testAdminAccessOnForbiddenNode() {
    $this->drupalLogin($this->adminUser);
    $this->createAccessFieldTypeOnEntityBundle('node', 'article', $this->fieldName, [Constants::OPERATION_EDIT => Constants::OPERATION_EDIT], Constants::FALLBACK_NEUTRAL, FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);
    $node = $this->createNode(['title' => 'Test Article', 'type' => 'article']);
    // Add our field:
    $this->setEntityAccessByRoleFieldOnEntity($node, $this->fieldName, ['authenticated'], Constants::ACCESS_FORBIDDEN);
    // Check expected access with our field:
    $this->drupalGet('/node/' . $node->id());
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('/node/' . $node->id() . '/edit');
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('/node/' . $node->id() . '/delete');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Check access on a node which allows authenticated users.
   */
  public function testAccessOnAllowedNodeAsAuth() {
    $this->drupalLogin($this->authenticatedUser);
    $this->createAccessFieldTypeOnEntityBundle('node', 'article', $this->fieldName, [Constants::OPERATION_EDIT => Constants::OPERATION_EDIT], Constants::FALLBACK_NEUTRAL, FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);
    $node = $this->createNode(['title' => 'Test Article', 'type' => 'article']);
    // Add our field:
    $this->setEntityAccessByRoleFieldOnEntity($node, $this->fieldName, ['authenticated'], Constants::ACCESS_ALLOWED);
    // Check expected access with our field:
    $this->drupalGet('/node/' . $node->id());
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('/node/' . $node->id() . '/edit');
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('/node/' . $node->id() . '/delete');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Check access on a node which forbids authenticated users.
   */
  public function testAccessOnForbiddenNodeAsAuth() {
    $this->drupalLogin($this->authenticatedUser);
    $this->createAccessFieldTypeOnEntityBundle('node', 'article', $this->fieldName, [Constants::OPERATION_EDIT => Constants::OPERATION_EDIT], Constants::FALLBACK_NEUTRAL, FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);
    $node = $this->createNode(['title' => 'Test Article', 'type' => 'article']);

    $this->setEntityAccessByRoleFieldOnEntity($node, $this->fieldName, ['authenticated'], Constants::ACCESS_FORBIDDEN);
    // Check expected access with our field:
    $this->drupalGet('/node/' . $node->id());
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('/node/' . $node->id() . '/edit');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet('/node/' . $node->id() . '/delete');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Check access on a node which forbids anonymous users, as a anonymous user.
   */
  public function testAccessOnForbiddenNodeAsGuest() {
    $this->createAccessFieldTypeOnEntityBundle('node', 'article', $this->fieldName, [Constants::OPERATION_EDIT => Constants::OPERATION_EDIT], Constants::FALLBACK_NEUTRAL, FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);
    // We create two entities here, as the access results otherwise
    // seem to be cached wrongly!
    $node = $this->createNode([
      'title' => 'Test Article2',
      'type' => 'article',
    ]);
    $this->setEntityAccessByRoleFieldOnEntity($node, $this->fieldName, ['anonymous'], Constants::ACCESS_FORBIDDEN);
    // Check expected access with our field:
    $this->drupalGet('/node/' . $node->id());
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('/node/' . $node->id() . '/edit');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet('/node/' . $node->id() . '/delete');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Check access on a node which allows test_role users, as a test_role user.
   */
  public function testAccessOnAllowedNodeAsNewRole() {
    $this->drupalLogin($this->testRoleUser);
    $this->createAccessFieldTypeOnEntityBundle('node', 'article', $this->fieldName, [Constants::OPERATION_EDIT => Constants::OPERATION_EDIT], Constants::FALLBACK_NEUTRAL, FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);
    $node = $this->createNode(['title' => 'Test Article', 'type' => 'article']);
    // Add our field:
    $this->setEntityAccessByRoleFieldOnEntity($node, $this->fieldName, ['test_role'], Constants::ACCESS_ALLOWED);
    // Check expected access with our field:
    $this->drupalGet('/node/' . $node->id());
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('/node/' . $node->id() . '/edit');
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('/node/' . $node->id() . '/delete');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Check access on a node which forbids test_role users, as a test_role user.
   */
  public function testAccessOnForbiddenNodeAsNewRole() {
    $this->drupalLogin($this->testRoleUser);
    $this->createAccessFieldTypeOnEntityBundle('node', 'article', $this->fieldName, [Constants::OPERATION_EDIT => Constants::OPERATION_EDIT], Constants::FALLBACK_NEUTRAL, FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);
    $node = $this->createNode(['title' => 'Test Article', 'type' => 'article']);
    // Add our field:
    $this->setEntityAccessByRoleFieldOnEntity($node, $this->fieldName, ['test_role'], Constants::ACCESS_FORBIDDEN);
    // Check expected access with our field:
    $this->drupalGet('/node/' . $node->id());
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('/node/' . $node->id() . '/edit');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet('/node/' . $node->id() . '/delete');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Check access on an allowed node's operation delete.
   */
  public function testAccessOnAllowedNodeWithOperationDelete() {
    $this->drupalLogin($this->authenticatedUser);
    $this->createAccessFieldTypeOnEntityBundle('node', 'article', $this->fieldName, [Constants::OPERATION_DELETE => Constants::OPERATION_DELETE], Constants::FALLBACK_NEUTRAL, FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);
    $node = $this->createNode(['title' => 'Test Article', 'type' => 'article']);
    // Add our field:
    $this->setEntityAccessByRoleFieldOnEntity($node, $this->fieldName, ['authenticated'], Constants::ACCESS_ALLOWED);
    // Check expected access with our field:
    $this->drupalGet('/node/' . $node->id());
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('/node/' . $node->id() . '/edit');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet('/node/' . $node->id() . '/delete');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Check access on an forbidden node's operation delete.
   */
  public function testAccessOnForbiddenNodeWithOperationDelete() {
    $this->drupalLogin($this->authenticatedUser);
    $this->createAccessFieldTypeOnEntityBundle('node', 'article', $this->fieldName, [Constants::OPERATION_DELETE => Constants::OPERATION_DELETE], Constants::FALLBACK_NEUTRAL, FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);
    $node = $this->createNode(['title' => 'Test Article', 'type' => 'article']);
    // Add our field:
    $this->setEntityAccessByRoleFieldOnEntity($node, $this->fieldName, ['authenticated'], Constants::ACCESS_FORBIDDEN);
    // Check expected access with our field:
    $this->drupalGet('/node/' . $node->id());
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('/node/' . $node->id() . '/edit');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet('/node/' . $node->id() . '/delete');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Check access on an allowed node's operation via unpublished.
   */
  public function testAccessOnAllowedNodeWithOperationViewUnpublished() {
    $this->drupalLogin($this->authenticatedUser);
    $this->createAccessFieldTypeOnEntityBundle('node', 'article', $this->fieldName, [Constants::OPERATION_VIEW_UNPUBLISHED => Constants::OPERATION_VIEW_UNPUBLISHED], Constants::FALLBACK_NEUTRAL, FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);
    $node = $this->createNode([
      'title' => 'Test Article',
      'type' => 'article',
      'uid' => $this->adminUser->id(),
      'status' => '0',
    ]);
    // Add our field:
    $this->setEntityAccessByRoleFieldOnEntity($node, $this->fieldName, ['authenticated'], Constants::ACCESS_ALLOWED);
    // Check expected access with our field:
    $this->drupalGet('/node/' . $node->id());
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('/node/' . $node->id() . '/edit');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet('/node/' . $node->id() . '/delete');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Check view access on an allowed node which is unpublished.
   */
  public function testAccessOnAllowedUnpublishedNodeWithOperationView() {
    $this->drupalLogin($this->authenticatedUser);
    $this->createAccessFieldTypeOnEntityBundle('node', 'article', $this->fieldName, [Constants::OPERATION_EDIT => Constants::OPERATION_EDIT], Constants::FALLBACK_NEUTRAL, FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);
    $node = $this->createNode([
      'title' => 'Test Article',
      'type' => 'article',
      'status' => '0',
    ]);
    // Add our field:
    $this->setEntityAccessByRoleFieldOnEntity($node, $this->fieldName, ['authenticated'], Constants::ACCESS_ALLOWED);
    // Check expected access with our field:
    $this->drupalGet('/node/' . $node->id());
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet('/node/' . $node->id() . '/edit');
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('/node/' . $node->id() . '/delete');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Check access on an forbidden node's operation vie unpublished.
   */
  public function testAccessOnForbiddenNodeWithOperationViewUnpublished() {
    $this->drupalLogin($this->authenticatedUser);
    $this->createAccessFieldTypeOnEntityBundle('node', 'article', $this->fieldName, [Constants::OPERATION_VIEW_UNPUBLISHED], Constants::FALLBACK_NEUTRAL, FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);
    $node = $this->createNode([
      'title' => 'Test Article',
      'type' => 'article',
      'status' => '0',
    ]);
    // Add our field:
    $this->setEntityAccessByRoleFieldOnEntity($node, $this->fieldName, ['authenticated'], Constants::ACCESS_FORBIDDEN);
    // Check expected access with our field:
    $this->drupalGet('/node/' . $node->id());
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet('/node/' . $node->id() . '/edit');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet('/node/' . $node->id() . '/delete');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Test fallback "allowed" with access allowed.
   */
  public function testAccessOnAllowedNodeWithFallbackAllowed() {
    $this->drupalLogin($this->authenticatedUser);
    $this->createAccessFieldTypeOnEntityBundle('node', 'article', $this->fieldName, [Constants::OPERATION_EDIT => Constants::OPERATION_EDIT], Constants::FALLBACK_ALLOWED, FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);
    $node = $this->createNode(['title' => 'Test Article', 'type' => 'article']);
    // Add our field:
    $this->setEntityAccessByRoleFieldOnEntity($node, $this->fieldName, [], Constants::ACCESS_ALLOWED);
    // Check expected access with our field:
    $this->drupalGet('/node/' . $node->id());
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('/node/' . $node->id() . '/edit');
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('/node/' . $node->id() . '/delete');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Test fallback "allowed" with access forbidden.
   */
  public function testAccessOnForbiddenNodeWithFallbackAllowed() {
    $this->drupalLogin($this->authenticatedUser);
    $this->createAccessFieldTypeOnEntityBundle('node', 'article', $this->fieldName, [Constants::OPERATION_EDIT => Constants::OPERATION_EDIT], Constants::FALLBACK_ALLOWED, FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);
    $node = $this->createNode(['title' => 'Test Article', 'type' => 'article']);
    // Add our field:
    $this->setEntityAccessByRoleFieldOnEntity($node, $this->fieldName, [], Constants::ACCESS_FORBIDDEN);
    // Check expected access with our field:
    $this->drupalGet('/node/' . $node->id());
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('/node/' . $node->id() . '/edit');
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('/node/' . $node->id() . '/delete');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Test fallback "forbidden" with access allowed.
   */
  public function testAccessOnAllowedNodeWithFallbackForbidden() {
    $this->drupalLogin($this->authenticatedUser);
    $this->createAccessFieldTypeOnEntityBundle('node', 'article', $this->fieldName, [Constants::OPERATION_EDIT => Constants::OPERATION_EDIT], Constants::FALLBACK_FORBIDDEN, FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);
    $node = $this->createNode(['title' => 'Test Article', 'type' => 'article']);
    // Add our field:
    $this->setEntityAccessByRoleFieldOnEntity($node, $this->fieldName, [], Constants::ACCESS_ALLOWED);
    // Check expected access with our field:
    $this->drupalGet('/node/' . $node->id());
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('/node/' . $node->id() . '/edit');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet('/node/' . $node->id() . '/delete');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Check access on a node which forbids test_role users, as a test_role user.
   */
  public function testAccessOnForbiddenNodeWithFallbackForbidden() {
    $this->drupalLogin($this->authenticatedUser);
    $this->createAccessFieldTypeOnEntityBundle('node', 'article', $this->fieldName, [Constants::OPERATION_EDIT => Constants::OPERATION_EDIT], Constants::FALLBACK_FORBIDDEN, FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);
    $node = $this->createNode(['title' => 'Test Article', 'type' => 'article']);
    // Add our field:
    $this->setEntityAccessByRoleFieldOnEntity($node, $this->fieldName, [], Constants::ACCESS_FORBIDDEN);
    // Check expected access with our field:
    $this->drupalGet('/node/' . $node->id());
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('/node/' . $node->id() . '/edit');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet('/node/' . $node->id() . '/delete');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Test to see if created roles are shown on creation of an article node.
   *
   * @todo This test is not working, fix it at some Point (remove todo in name).
   */
  public function todoTestNodeRoleFieldsExist() {
    $this->drupalLogin($this->adminUser);
    $this->createAccessFieldTypeOnEntityBundle('node', 'article', $this->fieldName, [Constants::OPERATION_EDIT => Constants::OPERATION_EDIT], Constants::FALLBACK_NEUTRAL, FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);
    \Drupal::service('entity_field.manager')->clearCachedFieldDefinitions();
    /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $display_repository */
    $display_repository = \Drupal::service('entity_display.repository');
    $display_repository->getFormDisplay('node', 'article')
      ->setComponent($this->fieldName, [
        'type' => 'default_entity_access_by_role_field_widget',
      ])
      ->save();
    $session = $this->assertSession();
    $this->drupalGet('/node/add/article');
    // $this->assertEquals('', $this->getSession()->getPage()->getHTML());
    $session->elementExists('css', "#edit-field-access-details-role-id-anonymous");
    $session->elementExists('css', "#edit-field-access-details-role-id-authenticated");
    $session->elementExists('css', "#edit-field-access-details-role-id-administrator");
    $session->elementExists('css', "#edit-field-access-details-role-id-role");
  }

  /**
   * Tests to see if the access fields exists.
   *
   * @todo This test is not working, fix it at some Point (remove todo in name).
   */
  public function todoTestNodeAccessFieldsExist() {
    $this->drupalLogin($this->adminUser);
    $this->createAccessFieldTypeOnEntityBundle('node', 'article', $this->fieldName, [Constants::OPERATION_EDIT => Constants::OPERATION_EDIT], Constants::FALLBACK_NEUTRAL, FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);
    \Drupal::service('entity_field.manager')->clearCachedFieldDefinitions();
    $session = $this->assertSession();
    $this->drupalGet('/node/add/article');
    $session->elementExists('css', "#edit-field-access-details-access-allowed");
    $session->elementExists('css', "#edit-field-access-details-access-forbidden");
  }

}
