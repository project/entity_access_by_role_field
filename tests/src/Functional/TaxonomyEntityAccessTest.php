<?php

namespace Drupal\Tests\entity_access_by_role_field\Functional;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\entity_access_by_role_field\Helper\Constants;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;

/**
 * This class provides access tests for entity_access_by_role_field taxonomy.
 *
 * @group entity_access_by_role_field
 */
class TaxonomyEntityAccessTest extends EntityAccessTestBase {
  use TaxonomyTestTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'entity_access_by_role_field',
    'taxonomy',
    'field',
  ];

  /**
   * {@inheritDoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Create the adminUser:
    $this->adminUser = $this->createUser([]);
    $this->adminUser->addRole($this->createAdminRole('admin', 'admin'));
    $this->adminUser->save();
    // Create the global permissionUser:
    $this->globalPermissionUser = $this->createUser([
      'bypass entity_access_by_role_field permissions',
    ]);
    // Create the authenticatedUser:
    $this->authenticatedUser = $this->createUser([]);
  }

  /**
   * Placeholder test.
   *
   * @todo This is a placeholder test, as long as we don't have any running
   * tests for the taxonomy tests. See
   * https://www.drupal.org/project/entity_access_by_role_field/issues/3359056.
   */
  public function testPlaceholder() {
    $this->assertEquals(1, 1);
  }

  /**
   * Check access on a taxonomy_term which allows authenticated users.
   *
   * @todo Fix this test.
   */
  public function todoTestAccessOnAllowedTaxonomyTerm() {
    $this->drupalLogin($this->authenticatedUser);
    $vocabulary = $this->createVocabulary();
    $this->createAccessFieldTypeOnEntityBundle($vocabulary->getEntityTypeId(), $vocabulary->bundle(), $this->fieldName, [Constants::OPERATION_VIEW => Constants::OPERATION_VIEW], Constants::FALLBACK_NEUTRAL, FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);
    $term = $this->createTerm($vocabulary);
    // Add our field:
    $this->setEntityAccessByRoleFieldOnEntity($term, $this->fieldName, ['authenticated'], Constants::ACCESS_ALLOWED);
    // Check expected access with our field:
    $this->drupalGet('/taxonomy/term/' . $term->id());
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('/taxonomy/term/' . $term->id() . '/edit');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet('/taxonomy/term/' . $term->id() . '/delete');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Check access on a taxonomy_term which forbids authenticated users.
   *
   * @todo Fix this test.
   */
  public function todoTestAccessOnForbiddenTaxonomyTerm() {
    $this->drupalLogin($this->authenticatedUser);
    $vocabulary = $this->createVocabulary();
    $this->createAccessFieldTypeOnEntityBundle($vocabulary->getEntityTypeId(), $vocabulary->bundle(), $this->fieldName, [Constants::OPERATION_VIEW => Constants::OPERATION_VIEW], Constants::FALLBACK_NEUTRAL, FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);
    $term = $this->createTerm($vocabulary);
    // Add our field:
    $this->setEntityAccessByRoleFieldOnEntity($term, $this->fieldName, ['authenticated'], Constants::ACCESS_FORBIDDEN);
    // Check expected access with our field:
    $this->drupalGet('/taxonomy/term/' . $term->id());
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet('/taxonomy/term/' . $term->id() . '/edit');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet('/taxonomy/term/' . $term->id() . '/delete');
    $this->assertSession()->statusCodeEquals(403);
  }

}
