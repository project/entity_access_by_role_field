<?php

namespace Drupal\Tests\entity_access_by_role_field\Functional;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\entity_access_by_role_field\Helper\Constants;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\BrowserTestBase;

/**
 * This class provides methods for access testing entity_access_by_role_field.
 *
 * @group entity_access_by_role_field
 */
abstract class EntityAccessTestBase extends BrowserTestBase {

  /**
   * The field name to test.
   *
   * @var string
   */
  protected $fieldName = 'field_access';

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * An admin user with all permissions.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $adminUser;

  /**
   * A user with the permission to skip field check.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $globalPermissionUser;

  /**
   * An authenticated user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $authenticatedUser;

  /**
   * A simple authenticated user with a test_role.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $testRoleUser;

  /**
   * Creates an Access field on an entity bundle type.
   *
   * @param string $entityTypeId
   *   The entity type id of the entity.
   * @param string $bundle
   *   The entity bundle to add the field on.
   * @param string $fieldName
   *   The field name of the field.
   * @param array $operationDefaults
   *   The allowed operations to access the instance of the entityTypeBundle.
   * @param string $emptyRoleFallbackDefaults
   *   The emptyRoleFallback value if no roles are selected.
   * @param null|int $cardinality
   *   The parameter which defines if there are multiple roles selected.
   */
  protected function createAccessFieldTypeOnEntityBundle(string $entityTypeId, string $bundle, string $fieldName, array $operationDefaults = [Constants::OPERATION_VIEW => Constants::OPERATION_VIEW], string $emptyRoleFallbackDefaults = Constants::FALLBACK_NEUTRAL, $cardinality = NULL) {
    // Create field:
    FieldStorageConfig::create([
      'field_name' => $fieldName,
      'entity_type' => $entityTypeId,
      'type' => Constants::FIELD_TYPE,
      'cardinality' => $cardinality ?? FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
    ])->save();

    // Create field instance:
    FieldConfig::create([
      'field_name' => $fieldName,
      'entity_type' => $entityTypeId,
      'bundle' => $bundle,
      'settings' => [
        'operations' => [
          Constants::OPERATION_EDIT => $operationDefaults[Constants::OPERATION_EDIT] ?? 0,
          Constants::OPERATION_VIEW => $operationDefaults[Constants::OPERATION_VIEW] ?? 0,
          Constants::OPERATION_VIEW_UNPUBLISHED => $operationDefaults[Constants::OPERATION_VIEW_UNPUBLISHED] ?? 0,
          Constants::OPERATION_DELETE => $operationDefaults[Constants::OPERATION_DELETE] ?? 0,
        ],
        'empty_roles_access_fallback' => $emptyRoleFallbackDefaults,
      ],
    ])->save();

    /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $display_repository */
    $display_repository = \Drupal::service('entity_display.repository');
    $display_repository->getFormDisplay($entityTypeId, $bundle)
      ->setComponent($fieldName, [
        'type' => 'default_entity_access_by_role_field_widget',
      ])
      ->save();

    $display_repository->getViewDisplay($entityTypeId, $bundle)
      ->setComponent($fieldName, [
        'type' => 'default_entity_access_by_role_field_formatter',
      ])
      ->save();

  }

  /**
   * Fills an entity access by role field with content.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity bundle instance to add the field on.
   * @param string $fieldName
   *   The field name.
   * @param array $roleIds
   *   The roleIds used in access.
   * @param string $access
   *   Role ids have access allowed or forbidden on the entity bundle instance.
   */
  protected function setEntityAccessByRoleFieldOnEntity(FieldableEntityInterface $entity, string $fieldName, array $roleIds, string $access) {
    $fieldValues = [];
    foreach ($roleIds as $roleId) {
      $fieldValues[] = [
        'role_id' => $roleId,
        'access' => $access,
      ];
    }
    $entity->set($fieldName, $fieldValues);
    $entity->save();
    return $entity;
  }

}
