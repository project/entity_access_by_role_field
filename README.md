# Entity Access by Role Field

## Feature Overview

- Provides a "Role access" field to determine access permissions per fielded entity by selected roles
- Selected roles can be allowed or denied access per entity
- Fallback behavior, if no role is selected, can be defined per field
- Entity access can be defined by allowed operation (view,edit,delete)
- Setting "View Unpublished" also allows access on unpublished entities if desired
- Access check can be skipped by global permission for permitted roles

*Initially based on [Entity Access by Role](https://www.drupal.org/project/entity_access_by_role) by [Shamrockonov](/u/shamrockonov "View user profile."). Thank you very very much, @Shamrockonov. We finally [decided to split this module off](#splitoff-reasons).*

### Reasons for split-off-fork

We decided to split this module of due to large code refactorings and missing maintainer response. Perhaps this will one day land in a 2.x entity\_access\_by\_role module, but due to changes in the field schema the upgrade path might become complicated.

## Requirements

This module does not require any other modules or libraries to work.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

- Create an "Entity Access by Role" field on an entity bundle.
- Define the "allowed operations to access this entity" and the
"Access for this entity, if no role is selected" settings.
- Define default values for "Allow / restrict role access to view this entity"
and "When the user has the following roles".
- Create an entity and define the "Allow / restrict role access to view this
entity" and "When the user has the following roles" settings in the widget.
- Done.

## Views Problem

This module implements [hook\_entity\_access()](https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Entity%21entity.api.php/function/hook_entity_access/9.1.x) to dynamically calculate entity access permissions. Due to the complex logic it doesn't implement [hook\_query\_TAG\_alter()](https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Database%21database.api.php/function/hook_query_TAG_alter/10). So **Views may display entities, which are "Access denied" for the users!** Take care, in some combination this may lead to information disclosure for such Views contents (e.g. seeing the label you should not see). Until the core issue (\[#777578\]) is fixed, you may try the views\_entity\_access\_check module if you're running into such cases: https://www.drupal.org/project/views\_entity\_access\_check And please help to push a core solution in the linked issue.

## Debugging permissions / entity access

For debugging permissions on entities, the following modules can be helpful:

1. [Devel](https://www.drupal.org/project/devel)
2. [Web Profiler](https://www.drupal.org/project/webprofiler)
3. [Drush Tools](https://www.drupal.org/project/drush_tools)
4. [Masquerade](https://www.drupal.org/project/masquerade)

## Alternative modules:

- https://www.drupal.org/project/access\_policy seems very powerful but also complex
- [Role Access Control](https://www.drupal.org/project/rac)
- [Entity Access by Role](https://www.drupal.org/project/entity_access_by_role)
- [Entity Access by Reference Field](https://www.drupal.org/project/entity_access_by_reference_field)
- [Private Entity](https://www.drupal.org/project/private_entity)
- [Personal Access Restriction](https://www.drupal.org/project/par)
- [Content Access](https://www.drupal.org/project/content_access)
- [Node View Permissions](https://www.drupal.org/project/node_view_permissions)
- [Access Policy](https://www.drupal.org/project/access_policy)

## Need to define access based on referenced entities instead?

Try our [Entity Access by Reference Field](https://www.drupal.org/project/entity_access_by_reference_field) module, which is based on the code and knowledge from this module to implement functionality similar to what we knew from [Node access node reference](/project/nodeaccess_nodereference) and [Node access user reference](https://www.drupal.org/project/nodeaccess_userreference) but that have no Drupal 8+ release.

## Maintainers

- Julian Pustkuchen - [Anybody](https://www.drupal.org/u/anybody)
- Joshua Sedler - [Grevil](https://www.drupal.org/u/grevil)
- Thomas Frobieter - [thomas.frobieter](https://www.drupal.org/u/thomasfrobieter)

## Supporting organizations

- [webks: websolutions kept simple](http://www.webks.de)
- [DROWL: Drupalbasierte Lösungen aus Ostwestfalen-Lippe (OWL)](http://www.drowl.de)
